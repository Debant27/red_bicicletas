const sgTransport = require('nodemailer-sendgrid-transport');
var nodemailer = require('nodemailer');

let mailConfig;
if(process.env.NODE_ENV === 'production'){
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
}
else{
    if(process.env.NODE_ENV === 'staging'){
        console.log('XXXXXXXXXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    }
    else{
        // todos los correos aqui son enviados por ethereal
        //Esta tecnica no sirve para usar en produccion
        
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_password
            }
        };
    }
}

module.exports = nodemailer.createTransport(mailConfig);